import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/_models/product';
import { ShoppingCartService } from 'src/app/_services/shopping-cart.service';
import { ShoppingCart } from 'src/app/_models/shopping-cart';

@Component({
  selector: 'product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent implements OnInit {
  @Input() product: Product;
  @Input() showActions = true;
  @Input() shoppingCart: ShoppingCart;

  constructor(private cartService: ShoppingCartService) { }

  ngOnInit(): void {
  }

  addToCart() {
    this.cartService.addToCart(this.product);
  }

}
