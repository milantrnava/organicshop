import { Component, OnInit, OnDestroy } from '@angular/core';
import { ShoppingCartService } from 'src/app/_services/shopping-cart.service';
import { ShoppingCart } from 'src/app/_models/shopping-cart';
import { subscribeOn } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { ShoppingCartItem } from 'src/app/_models/shopping-cart-item';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit, OnDestroy {
  cart$: ShoppingCart = new ShoppingCart(null);
  subscription: Subscription;

  constructor(private shoppingCartService: ShoppingCartService) { }

  async ngOnInit() {
    // this.cart$ = await this.shoppingCartService.getCart();
    // this.cart$.subscribe( temp => {
    //   let data: any;
    //   data = temp.payload.child('/items').val();
    //   let cart = new ShoppingCart(data);
    //   // this.shoppingCartItemCount = this.cart.totalItemsCount;
    //   console.log('data  ', data);
    //   console.log('cart  ', cart);
    // });

    this.subscription = (await this.shoppingCartService.getCartObs()).subscribe(cart => {
      this.cart$ = cart;
    });
  }

  clearCart() {
    this.shoppingCartService.clearCart();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}

