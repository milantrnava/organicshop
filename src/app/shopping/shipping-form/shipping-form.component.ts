import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Shipping } from 'src/app/_models/shipping';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/_services/auth.service';
import { Router } from '@angular/router';
import { OrderService } from 'src/app/_services/order.service';
import { Order } from 'src/app/_models/order';
import { ShoppingCart } from 'src/app/_models/shopping-cart';

@Component({
  selector: 'shipping-form',
  templateUrl: './shipping-form.component.html',
  styleUrls: ['./shipping-form.component.css']
})
export class ShippingFormComponent implements OnInit, OnDestroy {
  @Input() cart: ShoppingCart;
  shipping: Shipping = {} as Shipping;
  userId: string;  // chceme vedieť kto objednávku uložil
  subscription: Subscription;

  constructor(
    private router: Router,
    private authService: AuthService,
    private orderService: OrderService) { }

  ngOnInit(): void {
    // user$ je prihlásený user objekt z autentifikácie
    this.subscription = this.authService.user$.subscribe(user => this.userId = user.uid);
  }

  async placeOrder() {
    let order = new Order(this.userId, this.shipping, this.cart);
    let result = await this.orderService.placeOrder(order);

    this.router.navigate(['/order-success', result.key]); // firebase mi vráti key po uložení
    /* In this implementation, it is possible that the second line (for clearing the cart)
    fails for some unexpected reason while connecting with Firebase. A more reliable approach
    is to have a transaction. This will ensure that during placing an order, an order object
    is stored AND the corresponding shopping cart is cleared. Either both these operations
    succeed together or they both will fail. Using a transaction involves updating multiple
    nodes and that’s beyond the scope of this course. I’ve covered that in my course
    “Build Enterprise Applications with Angular”. */
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
