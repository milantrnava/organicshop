import { Component, OnDestroy, OnInit } from '@angular/core';
import { ProductService } from 'src/app/_services/product.service';
import { ActivatedRoute } from '@angular/router';
import { Product } from 'src/app/_models/product';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ShoppingCartService } from 'src/app/_services/shopping-cart.service';
import { ShoppingCart } from 'src/app/_models/shopping-cart';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit, OnDestroy {
  products: Product[] = [];
  filteredProducts: Product[] = [];
  productSubscription: Subscription;
  shoppingCartSubscription: Subscription;

  category; // : Category = {} as Category;
  cart: ShoppingCart;

  constructor(private route: ActivatedRoute, private productService: ProductService,
              private shoppingCartService: ShoppingCartService) {
  }

  /* constructor nemôže byť async, preto dávame toto do OnInit metódy,
  async pretože getCart vracia promise */
  async ngOnInit() {
    this.populateProducts();
    this.shoppingCartSubscription = (await this.shoppingCartService.getCart())
      .subscribe(cart => {
        let temp: any;
        temp = cart.payload.child('/items').val();
        this.cart = new ShoppingCart(temp);
      });
  }

  private populateProducts() {
    /* máme tu 2 async operácie a pri reloade by filtrovanie prebehlo ešte predtým
    ako by sa načítali všetky produkty. Aby sme nemuseli robiť tzv. nested subscription
    použijeme switchMap */
    this.productSubscription = this.productService.getAll()
    .pipe(switchMap(products => {
      this.products = products;
      return this.route.queryParamMap;
      // route.snapshot.queryParamMap...  snapshot nemusím použiť, lebo nebudem meniť DOM,
      // iba budem filtrovať Array objektov
      }
      )).subscribe(params => {
          this.category = params.get('category');
          this.applyFilter();
        });
  }

  private applyFilter() {
    this.filteredProducts = (this.category) ?
    this.products.filter(p => p.category === this.category) :
    this.products;
  }

  ngOnDestroy(): void {
    this.productSubscription.unsubscribe();
    this.shoppingCartSubscription.unsubscribe();
  }

}
