import { ShoppingCartItem } from './shopping-cart-item';
import { Product } from './product';

export class ShoppingCart {
  items: ShoppingCartItem[] = [];

  constructor(private itemsMap: { [productId: string]: ShoppingCartItem } ) {
    // z DB máme objekt ale chceme pracovať s array, týmto vytvoríme array z objektu
    this.itemsMap = this.itemsMap || {}; // null reference exeption
    // tslint:disable-next-line: forin
    for (let productId in itemsMap) {
      let item = itemsMap[productId];
      this.items.push(
        new ShoppingCartItem({...item, key: productId })
        ); // pridanie objektu do array, vytvoríme nový objekt, ktorý bude mať všetky properties z item a key
    }
   }

  // get productIds() {
  //   return Object.keys(this.items);  // javascript metóda ako vrátiť všetky keys objektu
  // }

  get totalItemsCount() {
      let count = 0;
      // tslint:disable-next-line: forin
      for (let productId in this.itemsMap) {
        count += this.itemsMap[productId].quantity;
      }  // postupne prechádza všetky produkty v košíku a sčítava množstvo
      return count;
  }

  get totalPrice() {
    let sum = 0;
    // tslint:disable-next-line: forin
    for (let productId in this.items) {
      sum += this.items[productId].totalPrice;
    }
    return sum;
  }

  getQuantity(product: Product) {
    let item = this.itemsMap[product.key];
    return item ? item.quantity : 0;
  }
}
