import { ShoppingCart } from './shopping-cart';

export class Order {
    datePlaced: number;
    items: any[];

    // shoppingCart nemusí byť public, pretože neukladáme tento objekt do vnútra order,
    // iba mapujeme položky s položkami v order. shoppingCart sa preto neobjaví ako property
    // po vytvorení inštancie order, properties hore a v konštriktore s public dekorátorom,
    // budú súčasťou objektu (inštancie order)
    constructor(public userId: string, public shipping: any, shoppingCart: ShoppingCart) {
        this.datePlaced = new Date().getTime();

        this.items = shoppingCart.items.map(i => {
            return {
              product: {
                title: i.title,
                imageUrl: i.imageUrl,
                price: i.price
              },
              quantity: i.quantity,
              totalPrice: i.totalPrice
            };
        });
    // použijem map pretože tentokrát to bude array objektov (položky objednávky)
    // a v DB to bude uložené s ID 0,1,2... čo sú indexy array.
    // shoppingCart.items je naproti tomu mapa s key's produktami, preto sa to musí
    // riešiť cez metódu map, nedá sa sem dať =
    }
}
