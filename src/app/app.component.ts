import { Component } from '@angular/core';
import { AuthService } from './_services/auth.service';
import { Router } from '@angular/router';
import { UserService } from './_services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private userService: UserService, private auth: AuthService, router: Router) {
    // pokaždé ak užívatel urobí login alebo logout toto observable emituje hodnotu
    auth.user$.subscribe(user => {
      if (user) {
        userService.save(user); // ak by user zmenil údaje cez google, pri vytvorení inštancie tohto comp. sa aktualizujú v db

        let returnUrl = localStorage.getItem('returnUrl');
        if (returnUrl) {
          localStorage.removeItem('returnUrl'); // chceme presmerovať len raz, pri reloadnutí stránky užívateľom
                                                // by ho to stále presmerovávalo
          router.navigateByUrl(returnUrl); // returnUrl bude napr."/check-out"
        }
      }
    });
    // unsubscribe nie je v tomto prípade potrebný, pretože app.component je root comp.
    // a vytvára sa iba single inštancia tohto comp. v DOM.

  }
}
