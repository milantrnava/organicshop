import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ngx-custom-validators';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DataTableModule } from 'angular7-data-table';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BsNavbarComponent } from './core/bs-navbar/bs-navbar.component';
import { HomeComponent } from './core/home/home.component';
import { ProductsComponent } from './shopping/products/products.component';
import { ShoppingCartComponent } from './shopping/shopping-cart/shopping-cart.component';
import { CheckOutComponent } from './shopping/check-out/check-out.component';
import { OrderSuccessComponent } from './shopping/order-success/order-success.component';
import { MyOrdersComponent } from './shopping/my-orders/my-orders.component';
import { AdminProductsComponent } from './admin/admin-products/admin-products.component';
import { AdminOrdersComponent } from './admin/admin-orders/admin-orders.component';
import { LoginComponent } from './core/login/login.component';
import { environment } from 'src/environments/environment';
import { AuthService } from './_services/auth.service';
import { AuthGuardService as AuthGuard } from './_services/auth-guard.service';
import { UserService } from './_services/user.service';
import { AdminAuthGuardService as AdminAuthGuard } from './_services/admin-auth-guard.service';
import { ProductFormComponent } from './admin/product-form/product-form.component';
import { CategoryService } from './_services/category.service';
import { ProductService } from './_services/product.service';
import { ProductFilterComponent } from './shopping/products/product-filter/product-filter.component';
import { ProductCardComponent } from './shared/product-card/product-card.component';
import { ShoppingCartService } from './_services/shopping-cart.service';
import { ProductQuantityComponent } from './shared/product-quantity/product-quantity.component';
import { OrderService } from './_services/order.service';
import { ShoppingCartSummaryComponent } from './shopping/shopping-cart-summary/shopping-cart-summary.component';
import { ShippingFormComponent } from './shopping/shipping-form/shipping-form.component';

@NgModule({
  declarations: [
    AppComponent,
    BsNavbarComponent,
    LoginComponent,
    HomeComponent,
    ProductsComponent,
    ShoppingCartComponent,
    CheckOutComponent,
    OrderSuccessComponent,
    MyOrdersComponent,
    AdminProductsComponent,
    AdminOrdersComponent,
    ProductFormComponent,
    ProductFilterComponent,
    ProductCardComponent,
    ProductQuantityComponent,
    ShoppingCartSummaryComponent,
    ShippingFormComponent
  ],

  imports: [
    BrowserModule,
    FormsModule,
    CustomFormsModule,
    NgbModule,
    DataTableModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    RouterModule.forRoot([
      { path: '', component: ProductsComponent }, // ako HomePage použijeme ProductsComp.
      { path: 'products', component: ProductsComponent },
      { path: 'shopping-cart', component: ShoppingCartComponent },
      { path: 'login', component: LoginComponent },

      { path: 'check-out', component: CheckOutComponent, canActivate: [AuthGuard] },
      { path: 'order-success/:id', component: OrderSuccessComponent, canActivate: [AuthGuard] },
      { path: 'my/orders', component: MyOrdersComponent, canActivate: [AuthGuard] },

      {
        path: 'admin/products/new',
        component: ProductFormComponent,
        canActivate: [AuthGuard, AdminAuthGuard]
      },
      {
        path: 'admin/products/:id',
        component: ProductFormComponent,
        canActivate: [AuthGuard, AdminAuthGuard]
      },
      {
        path: 'admin/products',
        component: AdminProductsComponent,
        canActivate: [AuthGuard, AdminAuthGuard]
      }, // cesty sa snažíme zoraďovať tak, aby najšpecifickejšia cesta bola na začiatku ('admin/products/new')
         // a najvšeobecnejšia na konci ('admin/products')
      {
        path: 'admin/orders',
        component: AdminOrdersComponent,
        canActivate: [AuthGuard, AdminAuthGuard]
      }
    ]),
    // AppRoutingModule
  ],

  providers: [
    // AuthService,
    // AuthGuard,
    // AdminAuthGuard,
    // UserService,
    // CategoryService,
    // ProductService,
    // ShoppingCartService,
    // OrderService
  ],

  bootstrap: [AppComponent]
})

export class AppModule { }
