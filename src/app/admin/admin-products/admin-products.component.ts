import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProductService } from 'src/app/_services/product.service';
import { Product } from 'src/app/_models/product';
import { Subscription } from 'rxjs';
import { DataTableResource } from 'angular7-data-table';

@Component({
  selector: 'app-admin-products',
  templateUrl: './admin-products.component.html',
  styleUrls: ['./admin-products.component.css']
})
export class AdminProductsComponent implements OnInit, OnDestroy {
  // products$; // kvôli filtrovaniiu už nemôžeme používať tieto observables
  products: Product[];
  // filteredProducts: Product[];
  subscription: Subscription;
  tableResource: DataTableResource<Product>;
  items: Product[] = [];   // záznamy na jednej page. = [] - kvôli chybe s lenght (item.lenght)
  itemCount: number;  // celkové množstvo záznamov


  constructor(private productService: ProductService) {
    // this.products$ = this.productService.getAll().snapshotChanges();

    // chceme nechať toto subscription počas celej životnosti tohto komponentu,
    // pretože je možné, že user bude mať otvorených viacero okien a chceme mať istotu,
    // že zmeny v ostatných oknách sa prejavia na zmene tohto listu produktov - musíme
    // ale použiť ngOnDestroy na unsubscribe
    this.subscription = this.productService.getAll()
      .subscribe(products => {
    // this.filteredProducts = this.products = products;
    this.products = products;

    this.initializeTable(products);
    });
   }

  ngOnInit(): void {
  }

  private initializeTable(products: Product[]) {
    this.tableResource = new DataTableResource(products);
    // query je metóda na získanie všetkých záznamov na jednu page na základe daných
    // parametrov, offset je číslo page - pri prvom zobrazení sa zobrazí page 1 = offset 0
    this.tableResource.query({ offset: 0 })
      .then(items => this.items = items);
    // count - celkový počet záznamov
    this.tableResource.count()
      .then(count => this.itemCount = count);
  }

  // spustí sa pokaždé keď niečo urobíme s tabuľkou, pagination, sorting...
  reloadItems(params) {
    // táto metóda sa spustí hned pri štarte komponentu, ešte skôr než sa stihne
    // inicializovať tabuľka, preto if
    if (!this.tableResource) { return; }

    this.tableResource.query({ offset: 0 })
    .then(items => this.items = items);
  }

  // riešenie filtrovania záleží na tom, či chceme filtrovať na serveri alebo klientovi.
  // Oshop bude mať iba menšie množstvo výrobkov a preto je výhodnejšie stiahnuť všetky
  // na klienta a filtrovať potom.
  filter(query: string) {
    let filteredProducts = (query) ?
    this.products.filter(p => p.title.toLowerCase().includes(query.toLowerCase())) :
    this.products;

    this.initializeTable(filteredProducts);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
