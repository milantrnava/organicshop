import { Component, OnDestroy } from '@angular/core';
import { CategoryService } from 'src/app/_services/category.service';
import { ProductService } from 'src/app/_services/product.service';
import { Router, ActivatedRoute } from '@angular/router';
import { take } from 'rxjs/operators';
import { Product } from 'src/app/_models/product';
import { Category } from 'src/app/_models/category';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent {
  categories$;
  id;
  product: Product = {} as Product;

  constructor(private categoryService: CategoryService,
              private productService: ProductService,
              private router: Router,
              private route: ActivatedRoute) {
    /* this.subscription = this.categoryService.getAll()
      .subscribe(caterories => this.categories = caterories); */
    this.categories$ = this.categoryService.getAll();

    this.id = this.route.snapshot.paramMap.get('id'); // zoberie z cesty id produktu.
                      // Id sa do cesty dostalo kliknutím na Edit v tabuľke produktov
    if (this.id) {
        this.productService.get(this.id).valueChanges()
        .pipe(take(1)).subscribe(p => this.product = p);
      }
   }

  save(product) {
    if (this.id) {
      this.productService.update(this.id, product);
    } else {
      this.productService.create(product);
    }
    this.router.navigate(['/admin/products']);
  }

  delete() {
    if (confirm('Are you sure you want to delete this product?')) {
      this.productService.delete(this.id);
      this.router.navigate(['/admin/products']);
    }
  }

}
