import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/_services/auth.service';
import { AppUser } from 'src/app/_models/app-user';
import { ShoppingCartService } from 'src/app/_services/shopping-cart.service';
import { ShoppingCart } from 'src/app/_models/shopping-cart';
import { Observable } from 'rxjs';

@Component({
  selector: 'bs-navbar',
  templateUrl: './bs-navbar.component.html',
  styleUrls: ['./bs-navbar.component.css']
})
export class BsNavbarComponent implements OnInit {
  appUser: AppUser;
  shoppingCartItemCount: number;
  // cart$: Observable<ShoppingCart>;

  constructor(private auth: AuthService, private shoppingCartService: ShoppingCartService) { }

  async ngOnInit() {
    this.auth.appUser$.subscribe(u => this.appUser = u);

    const cart$ = await this.shoppingCartService.getCart();
    cart$.subscribe(temp => {
      let data: any;
      data = temp.payload.child('/items').val();
      let cart = new ShoppingCart(data);
      this.shoppingCartItemCount = cart.totalItemsCount;
     });
    // Promise => async - await
    // v tomto komponente nepotrebujeme unsubscribe, lebo je počas celého behu apl.
    // iba jedna inštancia NavBaru
  }

  logout() {
    this.auth.logout();
  }
}
