import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app'; // toto by malo byť prvé.
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable, of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { UserService } from './user.service';
import { switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // keď pracujeme s firebase a jeho observable, musíme VŽDY použiť unsubscribe,
  // pretože to nie je ako v http servisoch, kde to Angular robí automaticky.
  // Mohli by sme použiť OnDestroy metódu ale dá sa to aj takto:
  // namiesto
    // user: firebase.User;
  // použijeme rxjs Async Pipe, čo je jednoduchšie a čistejšie,
  // táto Async Pipe automaticky použije unsubscribe:
  user$: Observable<firebase.User>; // prihlásený user objekt z autentifikácie

  constructor(private afAuth: AngularFireAuth, private route: ActivatedRoute,
              private userService: UserService) {
      // afAuth.authState.subscribe(user => this.user = user); - observable firebasu
      this.user$ = afAuth.authState; // observable rxjs
   }

  login() {
    // chceme získať z url queryParams
    const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || '/';  // ak neexistujú queryParams vráti '/'
    localStorage.setItem('returnUrl', returnUrl);
    // pokračovanie je v app.component.ts
    this.afAuth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());
  }

  logout() {
    this.afAuth.signOut();
  }

  // user (firebase.User) nie je náš user z DB ale prihlásený user objekt z autentifikácie - t.j. má iné polia
  // switchMap prepne z jedného observable (firebase) na druhé
  // appUser - user z DB
  // user -    user z autentifikácie
  get appUser$() {
    return this.user$
      .pipe(switchMap(user => {
        if (user) {
          return this.userService.get(user.uid).valueChanges(); // metóda valueChanges mi urobí z objektu observable
        } else {
          return of(null); // ak nie je user prihlásený vráti null, inak by nefungoval logout
        }
      }
      ));
  }

}
