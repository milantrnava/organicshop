import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireObject, AngularFireList } from '@angular/fire/database';
import { Product } from '../_models/product';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private db: AngularFireDatabase) { }

  create(product) {
    return this.db.list('/products').push(product);
  }

  getAll() {
    return this.db.list('/products').snapshotChanges()
    .pipe(map(actions => actions.map(a => ( { key: a.key, ...a.payload.val() as Product } ))));
  }

  get(productId): AngularFireObject<Product> {
    return this.db.object('/products/' + productId);
  }

  // firebase nedovolí mať objekt product aj s Id ale iba s údajmi, ktoré sa updatujú.
  // preto je argument productId zvlášť
  update(productId, product) {
    return this.db.object('/products/' + productId).update(product);
  }

  delete(productId) {
    return this.db.object('/products/' + productId).remove();
  }
}
