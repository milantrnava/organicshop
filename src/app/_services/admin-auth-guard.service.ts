import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AdminAuthGuardService implements CanActivate {

  constructor(private auth: AuthService, private userService: UserService) { }

  // interface canActivate má vrátiť:
  // Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree

  canActivate() {
    return this.auth.appUser$
      .pipe(map(appUser => appUser.isAdmin));  // pomocou map urobím z toku objektov appUser tok booleanov
  }
}
