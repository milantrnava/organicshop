import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject, SnapshotAction, DatabaseSnapshot, snapshotChanges } from '@angular/fire/database';
import { Product } from '../_models/product';
import { take, map } from 'rxjs/operators';
import { Item } from '../_models/item';
import { ShoppingCart } from '../_models/shopping-cart';
import { Observable } from 'rxjs';
import { ShoppingCartItem } from '../_models/shopping-cart-item';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {
  cart: any;
  items: ShoppingCartItem[];

  constructor(private db: AngularFireDatabase) { }

  async getCart() {
    const cartId = await this.getOrCreateCartId();
    return this.db.object('/shopping-carts/' + cartId).snapshotChanges();
  }

  async getCartObs() {
    const cartId = await this.getOrCreateCartId();
    return this.db.object('/shopping-carts/' + cartId).snapshotChanges()
    .pipe(map( temp => new ShoppingCart(temp.payload.child('/items').val()) ));
  }

  async addToCart(product: Product) {
    this.updateItem(product, 1);
    // await sem nemusím dávať, pretože nečakám na nejakú premenú alebo objekt,
    // ak by som dal napr.:  let x = await this...return...  potom by som await musel použiť
  }

  async removeFromCart(product: Product) {
    this.updateItem(product, -1);
  }

  async clearCart() {
    let cartId = await this.getOrCreateCartId(); // táto metóda nám vráti Promise, preto await
    this.db.object('/shopping-carts/' + cartId + '/items').remove();
  }

  /* toto vytvorí promise, ktorý vráti Id z DB (firebase db reference),
     Id využijeme ako cartId */
  private create() {
    return this.db.list('/shopping-carts').push({
      dateCreated: new Date().getTime()
    });
  }

  /* aby sme nemuseli použiť na promise z create() metódu .then(),
  môžeme poučiť async / await zápis. Z funkčného hľadiska sa nič nemení,
  budeme mať len prehľadnejší kód */
  private async getOrCreateCartId() {
    /* zistíme či existuje v localStorage cartId ak nie vytvoríme ho a vráti sa nám
    cartId, ktorý uložíme do localStorage. Nakoniec táto metóda vráti cartId */
    let cartId = localStorage.getItem('cartId');
    if (cartId) { return cartId; }

    let result = await this.create();
    localStorage.setItem('cartId', result.key);
    return result.key;
  }

  getAll() {
    return this.db.list('/products').snapshotChanges()
    .pipe(map(actions => actions.map(a => ( { key: a.key, ...a.payload.val() as Product } ))));
  }

  private getItem(cartId: string, productId: string) {
    return this.db.object('/shopping-carts/' + cartId + '/items/' + productId);
  }

  private async updateItem(product: Product, change: number) {
    let cartId = await this.getOrCreateCartId();
    // get request
    let item$ = this.getItem(cartId, product.key);
    item$.valueChanges().pipe(take(1)).subscribe((item: Item) => {
      if (!item) {
        // tslint:disable-next-line: object-literal-shorthand
        item$.set({
          title: product.title,
          imageUrl: product.imageUrl,
          price: product.price,
          quantity: 1
        });
      } else {
        let updatedQuantity = item.quantity + change;
        updatedQuantity === 0 ? item$.remove() : item$.update({ quantity: updatedQuantity});
      }
        /* uložíme do košíka celý objekt product a nie iba Id, pretože
        budeme potrebovať zoznam v košíku a dotazovať sa na všetky produkty z db cez Id je zložité. */
    });
  }

}
