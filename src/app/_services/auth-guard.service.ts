import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private auth: AuthService, private router: Router) { }

  canActivate(route, state: RouterStateSnapshot) {
    // rxjs map operátor používame preto, lebo ak by sme použili iba subscribe,
    // tak pretože sa jedná o servis, nie je tu OnDestroy aby bolo Observable automaticky unsubscribe.
    return this.auth.user$.pipe(map(user => {
      if (user) { return true; }

      this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } } );
      // pomocou queryParams chceme dosiahnuť aby ak nie som prihlásený a skúsim ísť cez address bar priamo
      // do napr. .../check-out tak ma to presmeruje na login stránku ale po logine ma to nenaviguje na homepage ale check-out
      // pridá to jednoducho do url posledne zadanú adresu
      return false;
    }));
  }
}
